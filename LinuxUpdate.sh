#!/bin/sh
sudo yum update -y --exclude WALinuxAgent kernel* 
sudo yum install -y epel-release
sudo yum install -y samba-common-tools
sudo yum group install -y "GNOME Desktop"
sudo ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target
sudo yum install -y xrdp
sudo systemctl enable xrdp && sudo systemctl start xrdp
sudo yum install R -y
sudo wget https://download2.rstudio.org/rstudio-server-rhel-1.1.453-x86_64.rpm
sudo yum install rstudio-server-rhel-1.1.453-x86_64.rpm -y
sudo systemctl status rstudio-server
